#include<stdio.h>
#include<stdlib.h>
#include"functions.h"

void readHeader(header_t *hdr, FILE *ptr)
{
  fscanf(ptr, "%s %d %d %d", hdr->magic, &hdr->width, &hdr->height,
                                &hdr->maxval);
}

void readImage(header_t	*hdr, pixel_t **pix, FILE*ptr)
{
	int i,j;

	for(i = 0; i < hdr->height; i++)
	{
		for(j = 0; j < hdr->width; j++)
		{
		fscanf(ptr, "%c%c%c", &pix[i][j].r, &pix[i][j].g, &pix[i][j].b);
		}
	}
}
void chooseTransform(header_t *hdr, pixel_t **pix, FILE *ptr){ 
    printf("+++++++++++++++++++++++++++++++++++++\n");
    printf("Thank you for using the Image Transform!\n");
    printf("There are several transformations you can perform\n");
    printf("on the input image. Choose the number that coorsponds to the\n");
    printf("transformations you wish to perform on the image!\n");
    printf("+++++++++++++++++++++++++++++++++++++\n");
    printf("1. Gray Scale\n");
    printf("2. Color to Negative\n");
    printf("3. Flip the Image\n");
    printf("4. Rotate Right\n");
    printf("5. Rotate Left\n");
    printf("6. Reprint\n");
    int num_get;
    scanf("%d",&num_get);
    if(num_get=1)
    {
    grayScaleImage(&hdr,pix,ptr);
    }else if(num_get=2){
    color2Negative(&hdr,pix,ptr);
    }else if(num_get=3){
    flipImage(&hdr,pix,ptr);
    }else if(num_get=4){
    rotateRight(&hdr,pix,ptr);
    }else if(num_get=5){
    rotateLeft(&hdr,pix,ptr);
    }else if(num_get=6){
    reprint(&hdr,pix,ptr);
    }else{
        printf("+++++++++++++++++++++++++++++++++++++\n");
        printf("You entered an incorrect number. Please,choose the\n");
        printf("number that coorsponds to the transformation you\n");
        printf("wish to perform on the image!\n");
        printf("+++++++++++++++++++++++++++++++++++++\n");
        printf("1. Gray Scale\n");
        printf("2. Color to Negative\n");
        printf("3. Flip the Image\n");
        printf("4. Rotate Right\n");
        printf("5. Rotate Left\n");
        printf("6. Reprint\n");
    }
}


/*
void printP6Image()
{rotateLeft()
	printf("2\n");
}
void grayScaleImage()
{
	printf("1\n");
}
void flipImage()
{
	printf("3\n");
}
void rotateLeft(){
	printf("5\n");
}
void rotateRight()
{
	printf("4\n");
}
void color2Negative()
{
	printf("2\n");
}
void reprint()
{
	printf("6\n");
}*/
